import pika
#
connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='logs', exchange_type='fanout')
result = channel.queue_declare(queue='', exclusive=True)
channel.queue_bind(exchange="logs", queue='')


def callback(ch, method, properties, body):
    print("body:%s" % body)


channel.basic_consume(queue='',on_message_callback=callback)

print("[*] waiting for message")
channel.start_consuming()
